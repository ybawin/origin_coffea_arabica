#!usr/bin/python3

#===============================================================================
# GIbPSs_SplitHybridLoci
#===============================================================================

# Yves BAWIN November 2018

#===============================================================================
# Import modules
#===============================================================================

import os, sys, argparse
from datetime import datetime

#===============================================================================
# Parse arguments
#===============================================================================

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Split the genome of hybrid samples in the GIbPSs output files based on genetic similarity with groups of putative progenitor samples.')

#Mandatory arguments
'''
Arguments defining file paths leading to the genotypes file, the locdata file, the table with progenitor samples, and the list with the hybrid samples.
'''
parser.add_argument('-gt', '--genotypes',
                    type = str,
                    help = 'Path to the genotypes file (usually called genotypes.txt).')
parser.add_argument('-l', '--locdata',
                    type = str,
                    help = 'Path to the locdata file (usually called locdata.txt).')
parser.add_argument('--progenitors',
                    type = str,
                    help = 'Path to a table (tab-delimited text file) with each column containing the sample names that belong to the same progenitor group (no header included).')
parser.add_argument('--hybrid',
                    type = str,
                    help = 'Path to a list (tab-delimited text file) with the sample names of a hybrid species (no header included).')


# Optional arguments
'''
Input data options
'''
parser.add_argument('--names',
                    type = str,
                    default = None,
                    help = 'List with the new sample names if the rename option is used: first column = old names, second column = new names (default = no list included).')
parser.add_argument('--Ploidy',
                    default = 2,
                    type = int,
                    help = 'Ploidy level of the progenitor species (default = 2 (diploid)).')

'''
Analysis options
'''
parser.add_argument('-c', '--completeness',
                    type = float,
                    default = 0,
                    help = 'Filter GIbPSs output files for locus completeness: loci with data in less samples than the predefined proportion are removed (default = no filtering for completeness).')
parser.add_argument('--polymorphic',
                    dest = 'Polymorphic',
                    action = 'store_true',
                    help = 'Ignore non-polymorphic loci (default = non-polymorphic loci are not ignored).')
parser.add_argument('--all_subgenomes',
                    dest = 'All_subgenomes',
                    action = 'store_true',
                    help = 'Only retain loci of hybrid samples if data are available for each subgenome (default = no filtering).')
parser.add_argument('--remove',
                    dest = 'removal',
                    action = 'store_true',
                    help = 'Remove taxa from the genotypes file. Only taxa that are listed in the file that is specified in the option "taxa", are kept (default = no taxa removed).')
parser.add_argument('--taxa',
                    type = str,
                    default = None,
                    help = 'List (tab-delimited text file) with sample names that must be retained (default = no list provided).')
parser.add_argument('--rename',
                    dest = 'rename',
                    action = 'store_true',
                    help = 'Rename samples as specified in the --taxa file: first column = old name, second column = new name (default = samples are not renamed).')

'''
Output data options
'''
parser.add_argument('-o', '--output_directory',
                    default = '.',
                    type = str,
                    help = 'Path to the output directory (default = current directory).')
parser.add_argument('--alignment_format',
                    choices = ['FastA', 'Phylip', 'Nexus']
                    default = 'FastA',
                    help = 'The format of the output alignment (default = FastA).')
parser.add_argument('--suffix',
                    type = str,
                    default = None,
                    help = 'Suffix for genotypes and locdata file after the split of hybrid genotypes (default = None).')
parser.add_argument('--separate_alignments',
                    dest = 'separation',
                    action = 'store_true',
                    help = 'Create separate locus alignments (default = concatenated alignment).')
parser.add_argument('--Delete_intermediate',
                    dest = 'Delete_intermediate_files',
                    action = 'store_true',
                    help = 'Delete the filtered genotypes and locdata file (default = intermediate files are not deleted).')

#Set default arguments.
parser.set_defaults(Polymorphic = False, rename = False, removal = False, Delete_intermediate_files = False, separation = False, All_subgenomes = False)

#Parse arguments to a dictionary.
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================
def print_date ():
    '''
    Print the current date and time to stderr.
    '''
    sys.stderr.write('-------------------\n')
    sys.stderr.write('{}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    sys.stderr.write('-------------------\n\n')
    return


def Filter(perc = args['completeness'], genotypes = args['genotypes'], locdata = args['locdata'], out_dir = args['output_directory'], taxa = args['taxa'], removal = args['removal'], rename = args['rename']):
    '''
    Remove samples from the genotypes and locdata file and filter for data completeness.
    * Two output files are created: genotypes_*complete_RC.txt and locdata_*complete_RC.txt.
    '''
    #Open the genotypes and the locdata file.
    genotypes = open(genotypes)
    locdata = open(locdata)
    
    #Create output files.
    genotypes_filtered = open('{}/genotypes_{}complete_RC.txt'.format(out_dir, perc), 'w+')
    locdata_filtered = open('{}/locdata_{}complete_RC.txt'.format(out_dir, perc), 'w+')
    
    #Create a dictionary with all sample names that must be retained (key = old sample name, value is new sample name).
    if removal or rename:
        taxa_dict = dict()
        taxa = open('{}/{}'.format(out_dir, taxa))
        for line in taxa:
            line = line.rstrip().split('\t')
            taxa_dict[line[0]] = ''
            if rename:
                taxa_dict[line[0]] = line[1]
    
    #Extract the header from the input files.
    header_genotypes = genotypes.readline()
    header_locdata = locdata.readline()
    
    #Remove taxa that are not included in the taxa dictionary.
    if removal:
        removed_taxa = list()
        header_genotypes = header_genotypes.rstrip().split('\t')
        for index, taxon in enumerate(header_genotypes[1:]):
            if taxon not in taxa_dict:
                header_genotypes[index + 1] = ''
                removed_taxa.append(index)
            elif rename:
                header_genotypes[index + 1] = taxa_dict[taxon]
        header_genotypes = list(filter(None, header_genotypes))
        
        #Print the header to the genotypes and locdata output file.
        print('\t'.join(taxon for taxon in header_genotypes), file = genotypes_filtered)
    else:
        print(header_genotypes, end = '', file = genotypes_filtered)
    print(header_locdata, end = '', file = locdata_filtered)
    
    #Remove the genotype scores of deleted samples and remove loci with only missing data.
    for samples, locus in zip(genotypes, locdata):
        data = 0
        samples = samples.rstrip().split('\t')
        if removal:
            for index, sample in enumerate(samples[1:]):
                if index in removed_taxa:
                    samples[index + 1] = ''
            samples = list(filter(None, samples))
        for genotype in samples[1:]:
            if genotype != '-999':
                data += 1
        
        #Print the genotypes and locus information to the output files.
        if data/len(samples[1:]) >= perc:
            print('\t'.join(sample for sample in samples), file = genotypes_filtered)
            print(locus, end = '', file = locdata_filtered)
    return


def Categorise(genotypes = args['genotypes'], locdata = args['locdata'], out_dir = args['output_directory'], progenitors = args['progenitors'], hybrid = args['hybrid'], polymorphic = args['Polymorphic'], perc = args['completeness'], suffix = args['suffix'], all_subgenomes = args['All_subgenomes']):
    '''
    Partition the loci of hybrid species into multiple sets of alleles based on the similarity with alleles of putative progenitors and/or their relatives.
    * Two output files are created: genotypes_*_complete(_polymorphic)_subgenome_assignment.txt and locdata_*_complete(_polymorphic)_subgenome_assignment.txt.
    '''
    
    #Open input files.
    genotypes = open('{}/genotypes_{}complete_RC.txt'.format(out_dir, perc))
    locdata = open('{}/locdata_{}complete_RC.txt'.format(out_dir, perc))
    
    #Create output files.
    genotypes_new = open('{}/genotypes_{}complete_subgenome_assignment{}{}.txt'.format(out_dir, perc, '_polymorphic' if polymorphic else '', '_{}'.format(suffix) if suffix else ''), 'w+')
    locdata_new = open('{}/locdata_{}complete_subgenome_assignment{}{}.txt'.format(out_dir, perc, '_polymorphic' if polymorphic else '', '_{}'.format(suffix) if suffix else ''), 'w+')
    
    #Open the progenitors table and the hybrids list.
    progenitors = open(progenitors)
    hybrid = open(hybrid)
    
    #Create for every progenitor group a list including all sample names that were assigned to that group.
    NumberOfProgenitors = len(progenitors.readline().rstrip().split('\t'))
    progenitors.seek(0)
    progenitor_names = [[] * x for x in range(NumberOfProgenitors)]
    for line in progenitors:
        line = line.rstrip().split('\t')
        for index, progenitor in enumerate(line):
            progenitor_names[index].append(progenitor)
    
    #Create a list including the names of the hybrid samples.
    hybrid_list = list()
    for line in hybrid:
       line = line.rstrip().split('\t')
       hybrid_list.append(line[0])
    
    #Extract the headers from the input files.
    header_genotypes = genotypes.readline()
    header_locdata = locdata.readline()
    
    #Print the locdata header to the locdata output file.
    print(header_locdata, end = '', file = locdata_new)
    
    #Extract in the genotypes header the indexes of the samples that represent the progenitor species.
    header_genotypes = header_genotypes.rstrip().split('\t')
    
    #Create a list containing a list for each progenitor with the indexes of the samples and a list with the indexes of the hybrid samples in the genotypes file.
    progenitor_indexes = [[] * x for x in range(NumberOfProgenitors)]
    hybrid_indexes = list()
    
    #Find the indexes of the samples in the progenitor and hybrid list.
    hybrid_names = list()
    for index, name in enumerate(header_genotypes[1:]):
        for i in range(NumberOfProgenitors):
            if name in progenitor_names[i]:
                progenitor_indexes[i].append(index)
        if name in hybrid_list:
            hybrid_indexes.append(index)
            hybrid_names.append(name)
            #Remove the names of the hybrid samples in the header.
            header_genotypes[index + 1] = ''
    header_genotypes = list(filter(None, header_genotypes))
    
    #Append for every hybrid sample and each subgenome a new header name.
    for name in hybrid_names:
        for i in range(NumberOfProgenitors):
            header_genotypes.append('{}_{}'.format(name, chr(97 + i).upper()))
    
    #Print the new header to the output file.
    print('\t'.join(name for name in header_genotypes), file = genotypes_new)
    
    #Iterate over the genotype calls in the genotypes file and check (i) whether the genotype calls are different between at least one sample of every progenitor and (ii) if the alleles of the hybrid could be assigned to one of the progenitors.
    for line, locus in zip(genotypes, locdata):
        line = line.rstrip().split('\t')
        #Create a list containing a list for every progenitor with the genotype calls of all corresponding samples and a list with the genotype calls of the hybrid species.
        progenitor_genotypes = [[] * x for x in range(NumberOfProgenitors)]
        hybrid_genotypes = list()
        for index, genotype in enumerate(line[1:]):
            for i in range(NumberOfProgenitors):
                if index in progenitor_indexes[i]:
                    progenitor_genotypes[i].append(genotype)
            if index in hybrid_indexes:
                hybrid_genotypes.append(genotype)
                line[index + 1] = ''
        
        #Check if the locus is variable and data is available for the hybrid samples and the progenitor samples.
        if not all([x == '-999' for x in hybrid_genotypes]) and not any([x == 'consensus' for x in hybrid_genotypes]):
            data = 0
            for i in range(NumberOfProgenitors):
                if not all([x == '-999' for x in progenitor_genotypes[i]]):
                    data += 1
            if data == NumberOfProgenitors:
                #Create a list to store the alleles that are assigned to each subgenome.
                subgenomes = list()
                #Iterate over the genotype calls of all hybrid samples and check if the alleles can be assigned to a particular progenitor species.
                for genotype in hybrid_genotypes:
                    subgenome_assignments = ['-999'] * NumberOfProgenitors
                    if genotype != '-999':
                        genotype = genotype.split('/')
                        assigned = 0
                        for allele in genotype:
                            #Create a list to store the minimum number of differences between a hybrid allele and an allele of a particular progenitor species.
                            minima = [len(allele)] * NumberOfProgenitors
                            #Evaluate the allele against each allele that is found for a particular progenitor species.
                            for i in range(NumberOfProgenitors):
                                for geno in progenitor_genotypes[i]:
                                    if geno != '-999':
                                        geno = geno.split('/')
                                        for al in geno:
                                            #Calculate the number of differences between the hybrid allele and all alleles in the progenitor samples. The smallest difference between a hybrid allele and an progenitor allele is added to the minima list.
                                            distance = sum(1 for a, b in zip(allele, al) if a != b)
                                            if distance < minima[i]:
                                                minima[i] = distance
                            minimum = min(minima)
                            if minima.count(minimum) == 1:
                                assigned += 1
                                if subgenome_assignments[minima.index(minimum)] == '-999':
                                    subgenome_assignments[minima.index(minimum)] = allele
                                else:
                                    subgenome_assignments[minima.index(minimum)] += '/{}'.format(allele)
                            
                    if (all_subgenomes and (subgenome_assignments.count('-999') > 0 or assigned < len(genotype))) or (not all_subgenomes and assigned < len(genotype)):
                        subgenome_assignments = ['-999'] * NumberOfProgenitors
                        
                    #Add the subgenome genotypes to the locus.
                    for call in subgenome_assignments:
                        subgenomes.append(call)
                
                #Print the genotypes and locus information to the output files.
                if subgenomes.count('-999') < ((1 - (perc)) * len(subgenomes)) :
                    for call in subgenomes:
                        line.append(call)
                    line = list(filter(None, line))
                    print('\t'.join(call for call in line), file = genotypes_new)
                    print(locus, end = '', file = locdata_new)
    return


def Template(alignment, locus_nr = None, out_dir = args['output_directory'], format = args['alignment_format'], perc = args['completeness'], sep = args['separation'], polymorphic = args['Polymorphic'], suffix = args['suffix']):
    '''
    Convert an alignment dictionary into a text file in the preferred alignment format.
    * An alignment file is created in fasta, phylip or nexus format.
    '''
    #Check whether the alignment format is supported by the script.
    assert format == 'FastA' or format == 'Phylip' or format == 'Nexus', 'The provided alignment format is not supported. Supported formats are FastA, Phylip, and Nexus.'
    
    #Create an output file
    out_file = open('{}/{}_{}complete_subgenome_assignment{}{}.{}'.format(out_dir, locus_nr if sep else 'concatenated_alignment', perc, '_polymorphic' if polymorphic else '', '_{}'.format(suffix) if suffix else '', 'nexus' if format == 'Nexus' else 'fasta' if format == 'FastA' else 'phy'), 'w+')
    
    #Calculate the number of samples and the length of the alignment.
    NumberOfNucleotides = len(alignment[list(alignment.keys())[0]])
    NumberOfSamples = len(alignment)
    
    #Print the Phylip header to the output file if the preferred alignment format is Phylip.
    if format == 'Phylip':
        print('{} {}'.format(NumberOfSamples, NumberOfNucleotides), file = out_file)
    
    #Print the Nexus header with the sample names to the output file if the preferred alignment format is Nexus.
    elif format == 'Nexus':
        print('#NEXUS\nBegin taxa;\n\tDimensions ntax={};\n\ttaxlabels'.format(NumberOfSamples), file = out_file)
        for name, sequence in alignment.items():
            print('\t{}'.format(name), file = out_file)
        print(';\nend;\n\nBegin characters;\n\tDimensions nchar={};\n\tformat datatype=dna missing=? gap=-;\n\tmatrix'.format(NumberOfNucleotides), file = out_file)
    
    #Print the alignment.
    for name, sequence in alignment.items():
        if sequence != '-' * len(sequence):
            if format == 'FastA':
                print('>{}\n{}'.format(name, sequence), file = out_file)
            elif format == 'Phylip':
                print('{}\t{}'.format(name, sequence), file = out_file)
            else:
                print('\t{}\t{}'.format(name, sequence), file = out_file)
    
    if format == 'Nexus':
        print(';\nend;', file = out_file)
    return
    
    
def CreateAlignment(out_dir = args['output_directory'], ploidy = args['Ploidy'], perc = args['completeness'], sep = args['separation'], Del_intermediate = args['Delete_intermediate_files'], polymorphic = args['Polymorphic'], suffix = args['suffix'], f  = args['alignment_format']):
    """
    Create an alignment dictionary using the GIbPSs genotypes and locdata file.
    """
    
    #Open the genotypes file and the locdata file.
    genotypes = open('{}/genotypes_{}complete_subgenome_assignment{}{}.txt'.format(out_dir, perc, '_polymorphic' if polymorphic else '', '_{}'.format(suffix) if suffix else ''))
    locdata = open('{}/locdata_{}complete_subgenome_assignment{}{}.txt'.format(out_dir, perc, '_polymorphic' if polymorphic else '', '_{}'.format(suffix) if suffix else ''))
    
    #Skip the first line in the locdata file
    locdata.readline()
    
    #extract the sample names from the genotypes header.
    sample_names = genotypes.readline().rstrip().split('\t')[1:]
    
    #Initiate an alignment dictionary (keys = sample name, values = sequence).
    alignment = dict()
    
    #Create a dictionary with all IUPAC codes
    IUPAC = {'A': 'A', 'C': 'C', 'T': 'T', 'G': 'G', 'AC': 'M', 'CT': 'Y', 'AT': 'W', 'AG': 'R', 'CG': 'S', 'GT': 'K', 'CTG': 'B', 'ATG':'D', 'ATC': 'H', 'ACG':'V', 'ACGT':'N', 'N':'N'}
    
    #Specify character for missing values.
    if f == 'Nexus':
        missing = '?'
    else:
        missing = '-'
    
    #Iterate over all loci in the locdata file
    for locus, samples in zip(locdata, genotypes):
        locus = locus.rstrip().split('\t')
        samples = samples.rstrip().split('\t')
        
        #Check whether the poplocID in the locdata file is the same as the poplocID in the genotypes file
        assert locus[0] == samples[0], 'Different poplocIDs in the genotypes file and the locdata file. Are you sure that you combined the correct input files?'
        
        #Check if a locus if polymorphic or not. Continue with the next locus if the locus is not polymorphic and only polymorphic loci are preferred.
        if polymorphic and 'consensus' in samples:
            continue
        
        #Define the number of nucleotides as the length of the entire locus.
        length = int(locus[1])
        
        #Make a list with SNP positions and split the list on slash.
        SNP_pos = locus[4][2:].split('/')
        
        #Iterate over the genotypes in one locus.
        for index, genotype in enumerate(samples[1:]):
            #Set the sequence to the consensus sequence.
            if genotype != '-999':
                sequence = locus[2].upper()
            
            #Change the sequence if the data are not present for the sample or if SNPs are present.
            if genotype != '-999' and genotype != 'consensus':
                #Split the variant positions on slash.
                genotype = genotype.split('/')
                #Create a new string for all SNPs encoded according to the IUPAC codes.
                nucleotides = ''
                #Combine the nucleotides that belong to the same position in the sequence in one string.
                for i in range(len(genotype[0])):
                    nucleotide = ''
                    for j in range(len(genotype)):
                        nucleotide += genotype[j][i]
                    #Remove doubles as a result of homozygous positions and sort the nucleotides alphabetically.
                    nucleotide = sorted(set(nucleotide.upper()))
                    
                    #Convert the variable position into N if the number of nucleotides is higher than the ploidy level.
                    if len(nucleotide) > ploidy or ('N' in nucleotide and len(nucleotide) > 1):
                        nucleotide = 'N'
                    nucleotide = ''.join(base for base in nucleotide)
                    nucleotides += IUPAC[nucleotide]
                
                sequence = list(sequence)
                for i in range(len(nucleotides)):
                    sequence[int(SNP_pos[i]) - 1] = nucleotides[i]
                sequence = ''.join(base for base in sequence)
                
            #Define sequence as a string of missing values if the genotype call is missing.
            elif genotype == '-999':
                sequence = missing * length
            
            #Add sequence to the alignment dictionary.
            if not sep or (sep and sequence != missing * length):
                if sample_names[index] not in alignment:
                    alignment[sample_names[index]] = sequence.upper()
                else:
                    alignment[sample_names[index]] += sequence.upper()
        
        #Create locus alignment.
        if sep:
            Template(alignment = alignment, locus_nr = locus[0])
            alignment = dict()
            
    #Create concatenated alignment.
    if not sep:
        Template(alignment, sample_names = sample_names)
    
    #Delete the filtered genotypes and locdata files if preferred
    if Del_intermediate:
        for file in os.listdir(out_dir):
            file_name = os.path.splitext(file)[0]
            if file_name.startswith('genotypes_{}complete'.format(perc)) or file_name.startswith('locdata_{}complete'.format(perc)):
                os.remove('{}/{}'.format(out_dir, file))
    return


#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':

    print_date()
    
    sys.stderr.write("* Filtering the genotypes file and the locdata file for a completeness of {} ...\n".format(args['completeness']))
    Filter()
    
    sys.stderr.write("* Assigning the alleles of the hybrid samples to a subgenome ... \n")
    Categorise()
    
    format = args['alignment_format']
    sys.stderr.write("* Converting the filtered genotypes file and the filtered locdata file into {}{} alignment{} ...\n".format('a concatenated ' if not args['separation'] else 'a ', 'Phylip' if format == 'Phylip' else 'Nexus' if format == 'Nexus' else 'FastA', ' for each locus' if args['separation'] else ''))
    CreateAlignment()
    
    sys.stderr.write("* Finished! \n\n")
    print_date()
                        