#!/usr/bin/python3

#===============================================================================
# GIbPSs_CalculateGeneticRelatedness
#===============================================================================

# Yves Bawin, February 2019

#===============================================================================
# Import modules
#===============================================================================

import os, sys, argparse, math, random
from datetime import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import shelve
import seaborn as sns

#===============================================================================
# Parse arguments
#===============================================================================

#Create an ArgumentParser object.
parser = argparse.ArgumentParser(description = 'Calculate pairwise genetic similarity or genetic distance values between all specified samples in the GIbPSs output file "genotypes.txt".')

#Add mandatory arguments.
parser.add_argument('-gt', '--genotypes',
                    type = str,
                    help = 'Path to the genotypes file (usually called genotypes.txt).')

#Add optional arguments
'''Input data options'''
parser.add_argument('-i', '--input_directory',
                    default = '.',
                    type = str,
                    help = 'Input directory (default = current directory).')
parser.add_argument('--names',
                    type = str,
                    default = None,
                    help = 'List (tab delimited text file) with (a subset of) the sample names in the first column and the new sample names in the second column (default = no list provided).')

'''Analysis options'''
parser.add_argument('--include_unique_loci',
                    dest = 'common_loci_only',
                    action = 'store_false',
                    help = 'Include loci in the genetic similarity/distance calculations that are unique for one of the samples in a sample pair (default = only common loci are used for similarity/distance calculation).')
parser.add_argument('-s', '--similarity_coefficient',
                    choices = ['Jaccard', 'Sorensen-Dice', 'Ochiai', 'Common_allele'],
                    default = 'Jaccard',
                    help = 'Coefficient used to express pairwise genetic similarity between samples (default = Jaccard).')
parser.add_argument('--distance',
                    dest = 'distance_calculation',
                    action = 'store_true',
                    help = 'Convert genetic similarities into genetic distances (default = no conversion to distances).')
parser.add_argument('--distance_method',
                    choices = ['Inversed', 'Euclidean'],
                    default = 'Inversed',
                    help = 'Method used for conversion of similarity estimates to genetic distances (default = Inversed).')

'''Output data options.'''
parser.add_argument('-o', '--output_directory',
                    default = '.',
                    type = str,
                    help = 'Output directory (default = current directory).')
parser.add_argument('--format',
                    choices = ['Phylip', 'Nexus'],
                    default = 'Phylip',
                    help = 'The format of the genetic similarity or distance matrix (default = Phylip).')
parser.add_argument('--plot',
                    dest = 'plot_estimates',
                    action = 'store_true',
                    help = 'Create plots of the similarity/distance matrix (default = no plots).')
parser.add_argument('--plot_list',
                    type = str,
                    default = None,
                    help = 'Use this option if you want to plot the genetic relatedness of a sample to all other samples in function of the number of loci. This option needs a list (tab-delimited text file) with the sample names for which plots must be created (default = no list included).')
parser.add_argument('--plot_interval',
                    type = int,
                    default = 10,
                    help = 'Interval between two consecutive points in the plot (default = 10).')
parser.add_argument('--plot_format',
                    choices = ['pdf', 'png', 'svg'],
                    default = "pdf",
                    help = 'File format of plots (default = pdf).')
parser.add_argument('--mask_upper',
                    dest = 'mask_upper_diagonal', 
                    action = 'store_true',
                    help = 'Mask values on and above the main diagonal of the similarity/distance matrix (default = no masking).')
parser.add_argument('--annotate_heat_map',
                    dest = 'add_values', 
                    action = 'store_true',
                    help = 'Annotate the heat map with genetic similarity/distance values (default = heatmap not annotated).')
parser.set_defaults(mask_upper_diagonal=False, add_values = False)

#Parse arguments to a dictionary.
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================
def print_date ():
    '''
    Print the current date and time to stderr.
    '''
    sys.stderr.write('----------------\n')
    sys.stderr.write('{}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M')))
    sys.stderr.write('----------------\n\n')
    return


def Calc_allele_similarity(ind1_name, ind2_name, ind1, ind2, plots, labels, names_list, colour, nloci, s = args['similarity_coefficient'], common_loci = args['common_loci_only'], dist = args['distance_calculation'], plot_int = args['plot_interval']):
    '''
    Count the number of common alleles and unique alleles in two samples.
    '''
    #Create lists to store intermediate calculations for plotting.
    if plots:
        estimates = [0]
        com_loc = [0]
    
    #Determine the number of common loci (l), common alleles (a), unique alleles for ind1 (b), and unique alleles for ind2 (c) between two individuals.
    a, b, c, e, l, u = 0, 0, 0, 0, 0, 0
    for gen1, gen2 in random.sample(list(zip(ind1, ind2)), len(ind1)):
        #Count common and unique alleles if both samples have locus data.
        if gen1 != '-999' and gen2 != '-999':
            l += 1
            gen1 = gen1.split('/')
            gen2 = gen2.split('/')
            com_al = list()
            for allele in gen1:
                if allele in gen2:
                    com_al.append(allele)
                else:
                    b += 1
            for allele in gen2:
                if allele not in gen1:
                    c += 1
            if len(com_al) > 0:
                e += 1
                a += len(com_al)
        
        #Count the number of unique alleles in unique loci if these loci are also taken into account (common_loci = False).
        elif not common_loci:
            u += 1
            if gen1 != '-999' and gen2 == '-999':
                b += len(gen1.split('/'))
            elif gen1 == '-999' and gen2 != '-999':
                c += len(gen2.split('/'))
        
        #Calculate relatedness value every x loci (defined by the plot_interval argument) and add them to plot lists (optional).
        if plots and l % plot_int == 0 and com_loc[-1] != l:
            d = Calc_relatedness(a, b, c, e, l)
            estimates.append(d)
            com_loc.append(l + u)
    
    #Calculate genetic relatedness between samples and plot their values in function of considered loci (optional).
    d = Calc_relatedness(a, b, c, e, l)
    if plots and ind1_name != ind2_name:
        if l % plot_int != 0:
            estimates.append(d)
            com_loc.append(l + u)
        labels.append(ind2_name)
        nloci = plot(estimates, com_loc, ind1_name, ind2_name, labels, names_list, colour, nloci)
    return d, l, nloci


def Calc_relatedness(a, b, c, e, l, s = args['similarity_coefficient'], dist = args['distance_calculation'], d_method = args['distance_method']):
    '''
    Calculate genetic similarity or distance between values.
    '''

    #Calculate genetic similarity between two samples.
    if a + b + c != 0:
        if s == 'Jaccard':
            d = a / (a + b + c)
        elif s == 'Common_allele':
            d = e / l
        elif s == 'Sorensen-Dice':
            d = (2 * a) / ((2 * a) + b + c)
        elif s == 'Ochiai':
            d = a / math.sqrt((a + b) * (a + c))
    else:
        d = 0
    
    #Optional: calculate genetic distance between two samples.
    if dist:
        if d_method == 'Euclidean':
            d = math.sqrt((1 - d)**2)
        else:
            d = 1 - d
    return d


def plot(estimates, comloc, ind1_name, ind2_name, labels, names_list, colour, nloci, s = args['similarity_coefficient'], distance = args['distance_calculation'], distance_method = args['distance_method'], f =args['plot_format'], plot_int = args['plot_interval'], out_dir = args['output_directory']):
    '''
    Plot genetic relatedness values in function of the number of loci.
    '''
    new_rc_params = {'text.usetex': False, 'svg.fonttype': 'none'}
    plt.rcParams.update(new_rc_params)
    plt.plot(comloc[1:], estimates[1:], linewidth = 1.0, c = colour)
    if max(comloc) > nloci:
        nloci = max(comloc)
    if names_list.index(ind2_name) + 1 == len(names_list):
        plt.title('{} {} estimates\n{}'.format(s, '{} distance'.format('Inversed' if distance_method == 'Inversed' else 'Euclidean') if distance else 'similarity', ind1_name))
        plt.legend(labels, fontsize = 7)
        plt.xlabel('Number of loci', fontsize = 12)
        plt.ylabel('{} coefficient estimate'.format(s), fontsize = 12)
        plt.xlim(0, nloci + 4000)
        plt.ylim(0, 1)
        if f == 'pdf' :
            plt.savefig('{}/{}_{}_{}.pdf'.format(out_dir, s, '{}_distance'.format(distance_method) if distance else 'similarity', ind1_name), format='pdf')
        elif f == 'png':
            plt.savefig('{}/{}_{}_{}.png'.format(out_dir, s, '{}_distance'.format(distance_method) if distance else 'similarity', ind1_name), format='png')
        else:
            plt.savefig('{}/{}_{}_{}.svg'.format(out_dir, s, '{}_distance'.format(distance_method) if distance else 'similarity', ind1_name), format='svg')
        plt.close()
    return nloci


#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':
    print_date()
    '''
    Part 1: Read and store data.
    '''
    #Import genotypes.txt as dataframe.
    genotypes = pd.read_table('{}/{}'.format(args['input_directory'], args['genotypes']), sep='\t',index_col=0,dtype=str)
    
    #Transpose the dataframe (rows = sample names, columns = loci).
    transposed = genotypes.transpose()
    
    #Create a tuple containing all individuals.
    tuple_indvs = transposed.index.values.tolist()
    
    #Create a list with all sample names that are included in the file defined by the '--names' argument.
    names_list = list()
    if args['names']:
        names_file = open(args['names'])
        for name in names_file:
            name = name.rstrip().split('\t')
            assert name[0] in tuple_indvs, 'Sample {} is not found in the genotypes table. Please check if the names in the names file exactly match with the sample names in the header of the genotypes table.'.format(name[0])
            names_list.append((name[0], name[1]))
    else:
        names_list = tuple_indvs
    
    #Open the list with the samples for plotting relatedness values in function of the number of loci.
    if args['plot_list']:
        plot_samples = list()
        plot_list = open(args['plot_list'])
        for sample in plot_list:
            sample = sample.rstrip()
            if args['names']:
                assert sample in [x[1] for x in names_list], 'Sample {} is not found in the sample list. Please check if the names in the plot_list exactly match with the sample names in the header of the Genotypes table.'.format(sample)
            else:
                assert sample in names_list, 'Sample {} is not found in the sample list. Please check if the names in the plot_list exactly match with the sample names in the names in the list defined by the "--names" option.'.format(sample)
            plot_samples.append(sample)
    
    #Initiate a new shelve file to store the genotypes data.
    shelf = shelve.open('{}/Genotypes.shelve'.format(args['input_directory']), 'c')
    
    #Loop over all individuals and add the individual name as key and a tuple containing the genotype calls as value to the shelve.
    sys.stderr.write(' * Converting the genotypes file into a shelve ...\n')
    for sample in names_list:
        if args['names']:
            shelf[sample[1]] = transposed.iloc[tuple_indvs.index(sample[0])].values.tolist()
            
        else:
            shelf[sample] = transposed.iloc[tuple_indvs.index(sample)].values.tolist()
    shelf.close()
    
    #Convert the sample list into a list that only contains the new names.
    if args['names']:
        names_list = [sample[1] for sample in names_list]
    
    '''
    Part 2: Calculate Genetic relatedness between samples and create plots.
    '''
    #Create new dataframes for the genetic relatedness and for the number of common loci.
    df = pd.DataFrame(index = names_list)
    cl = pd.DataFrame(index = names_list)
    
    #Iterate over all samples and calculate pairwise relatedness values.
    sys.stderr.write(' * Calculating {} similarities{} ...\n'.format(args['similarity_coefficient'], ' and converting them into {} distances'.format(args['distance_method']) if args['distance_calculation'] else ''))
    with shelve.open('{}/Genotypes.shelve'.format(args['input_directory']), 'r') as shelf:
        colours = ["black","grey","darkgrey","lightgrey","darkcyan","cyan","olive","olivedrab","magenta","darkmagenta","plum","maroon","darkslategrey","orange","darkorange","aquamarine","mediumaquamarine","gold","lemonchiffon","azure","green","lightgreen","darkgreen","orchid","darkorchid","mediumorchid","darkred","red","firebrick","royalblue","lightsteelblue","purple","darkseagreen","seagreen","lightseagreen","orangered","lightsalmon","lime","saddlebrown","sandybrown","peachpuff","khaki","darkkhaki","blue","lightblue","darkblue","yellow","lightcoral","indianred"]
        #colours = set()
        while len(colours) < len(names_list):
            colours.add("#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)]))
        colours = list(colours)
        nloci = 0
        
        #Iterate over all samples.
        for ind1 in names_list:
            #Set plots if individual is in plot_list.
            plots = False
            if args['plot_list']:
                if ind1 in plot_samples:
                    plots = True
                    plt.figure(figsize = (10,10), dpi = 300)
            
            #Create intermediate lists to store the calculated relatedness values and the number of common loci.
            distances = list()
            common_loci = list()
            labels = list()
            
            #Calculate genetic relatedness and the number of common loci.
            for ind2 in names_list:
                d, l, nloci = Calc_allele_similarity(ind1, ind2, shelf[ind1], shelf[ind2], plots, labels, names_list, colours[names_list.index(ind2)], nloci)
                distances.append(d)
                common_loci.append(l)
                
            #Add the calculated values to the dataframes.
            df[ind1] = distances
            cl[ind1] = common_loci
    
    shelf.close()
    
    #Write genetic relatedness values to an output Phylip or Nexus file.
    sys.stderr.write(' * Writing output tables ...\n')
    if args['format'] == 'Nexus':
        file = open('{}/{}_{}.nex'.format(args['output_directory'], 'Distances', args['similarity_coefficient']), 'w+')
        print('#nexus\nBEGIN Taxa;\nDIMENSIONS ntax={};\nTAXLABELS'.format(len(names_list)), file = file)
        for index, sample in enumerate(names_list):
            print("[{}] '{}'".format(index + 1, sample), file = file)
        print(';\nEND; [Taxa]\nBEGIN Distances;\nDIMENSIONS ntax={};\nFORMAT labels=left diagonal triangle=both;\nMATRIX'.format(len(names_list)), file = file)
        for index, sample in enumerate(names_list):
            print("[{}] '{}' {}".format(index + 1, sample, ' '.join(str(e) for e in df[sample]).lstrip()), file = file)
        print(';\nEND; [Distances]', file = file)
    else:
        df.to_csv('{}/{}_{}.dist'.format(args['output_directory'], '{} Distances'.format(args['distance_method']) if args['distance_calculation'] else 'Similarities', args['similarity_coefficient']), sep = '\t')
    
    #Write the number of common loci between sample pairs to an output text file.
    cl.to_csv('{}/Common_loci.txt'.format(args['output_directory']), sep = '\t')
    
    #Create heat maps for the genetic relatedness values and the number of common loci.
    if args['plot_estimates']:
        #Create heat map for genetic relatedness values.
        data = df.values
        width = int(data.shape[0])
        height = int(data.shape[1])
        fig, ax = plt.subplots(figsize=(width + 5, height + 5))
        new_rc_params = {'text.usetex': False, 'svg.fonttype': 'none'}
        plt.rcParams.update(new_rc_params)
        
        #Mask upper diagonal of the matrix (optional).
        sns.set(font_scale=2)
        if args['mask_upper_diagonal']:
            mask = np.zeros_like(df)
            mask[np.triu_indices_from(mask)] = True
            ax = sns.heatmap(df, linewidths=0.5, mask=mask, cmap="bone", vmin=0, vmax=1)
        else:
            ax = sns.heatmap(df, linewidths=0.5, cmap="bone", vmin=0, vmax=1)
        
        #Adjust label and background settings.
        ax.tick_params(axis='both', which='both', length=0, labelsize=12)
        ax.set_facecolor('w')
        
        #Print values to the output file (optional).
        if args['add_values']:
            for y in range(data.shape[0]):
                for x in range(data.shape[1]):
                    if not args['mask_upper_diagonal'] or x < y:
                        if float('%.2f' % data[y, x]) < 0.50:
                            plt.text(x + 0.5, y + 0.5, '%.2f' % data[y, x], horizontalalignment='center', verticalalignment='center', color='w', fontsize=12)
                        else:
                            plt.text(x + 0.5, y + 0.5, '%.2f' % data[y, x], horizontalalignment='center', verticalalignment='center', color='k', fontsize=12)
        
        #Save heat map.
        if args['plot_format'] == 'pdf':
            fig.savefig('{}/{}_{}_heatmap{}.pdf'.format(args['output_directory'], args['similarity_coefficient'], '{}_distance'.format(args['distance_method']) if args['distance_calculation'] else 'similarity', '_annot' if args['add_values'] else ''), format='pdf')
        elif args['plot_format'] == 'png':
            fig.savefig('{}/{}_{}_heatmap{}.png'.format(args['output_directory'], args['similarity_coefficient'], '{}_distance'.format(args['distance_method']) if args['distance_calculation'] else 'similarity', '_annot' if args['add_values'] else ''), format='png')
        else:
            fig.savefig('{}/{}_{}_heatmap{}.svg'.format(args['output_directory'], args['similarity_coefficient'], '{}_distance'.format(args['distance_method']) if args['distance_calculation'] else 'similarity', '_annot' if args['add_values'] else ''), format='svg')
        plt.close()
        
        #Create heat map for the number of common loci.
        data = cl.values
        max_data = int([max(x) for x in data][0])
        width = int(data.shape[0])
        height = int(data.shape[1])
        fig, ax = plt.subplots(figsize=(width + 5, height + 5))
        new_rc_params = {'text.usetex': False, 'svg.fonttype': 'none'}
        plt.rcParams.update(new_rc_params)
        
        #Mask upper diagonal of the matrix (optional).
        sns.set(font_scale=2)
        if args['mask_upper_diagonal']:
            mask = np.zeros_like(data)
            mask[np.triu_indices_from(mask,1)] = True
            ax = sns.heatmap(cl, linewidths=0.5, mask=mask, cmap="bone_r", vmin=0, vmax=max_data)
        else:
            ax = sns.heatmap(cl, linewidths=0.5, cmap="bone_r", vmin=0, vmax=max_data)
        
        #Adjust label and background settings.
        ax.tick_params(axis='both', which='both', length=0, labelsize=12)
        ax.set_facecolor('w')
        
        #Print values to the output file (optional).
        if args['add_values']:
            for y in range(data.shape[0]):
                for x in range(data.shape[1]):
                    if not args['mask_upper_diagonal'] or x <= y:
                        if float(data[y, x]) >= max_data / 3:
                            plt.text(x + 0.5, y + 0.5, data[y, x], horizontalalignment='center', verticalalignment='center', color='w', fontsize=12)
                        else:
                            plt.text(x + 0.5, y + 0.5, data[y, x], horizontalalignment='center', verticalalignment='center', color='k', fontsize=12)
        
        #Save heat map.
        if args['plot_format'] == 'pdf':
            fig.savefig('{}/common_loci_heatmap{}.pdf'.format(args['output_directory'], '_annot' if args['add_values'] else ''), format='pdf')
        elif args['plot_format'] == 'png':
            fig.savefig('{}/Common_loci_heatmap{}.png'.format(args['output_directory'], '_annot' if args['add_values'] else ''), format='png')
        else:
            fig.savefig('{}/Common_loci_heatmap{}.svg'.format(args['output_directory'], '_annot' if args['add_values'] else ''), format='svg')
        plt.close()
    
    sys.stderr.write(' * Finished!\n\n')
    print_date()