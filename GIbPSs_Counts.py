#!usr/bin/python3

#===============================================================================
# GIbPSs_Counts
#===============================================================================

# Yves BAWIN July 2019

#===============================================================================
# Import modules
#===============================================================================

import sys, argparse
from datetime import datetime

#===============================================================================
# Parse arguments
#===============================================================================

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Count the number of loci, nucleotide sites, and SNPs in (a selection of samples) in the GIbPSs output files.')

#Mandatory arguments
'''
Arguments for the file paths leading to the genotypes file and the locdata file.
'''
parser.add_argument('-gt', '--genotypes',
                    type = str,
                    help = 'Path to the genotypes file (usually called genotypes.txt).')
parser.add_argument('-l', '--locdata',
                    type = str,
                    help = 'Path to the locdata file (usually called locdata.txt).')

#Optional arguments
'''
Input data options
'''
parser.add_argument('--names',
                    default = None,
                    type = str,
                    help = 'List of all sample names (tab delimited text file) that must be taken into account for locus and SNP counts (default = no list included).')

'''
Analysis options
'''
parser.add_argument('-c', '--completeness',
                    default = 0,
                    type = float,
                    help = 'Filter GIbPSs output files for locus completeness: loci with data in less samples than the predefined proportion are removed (default = no filtering).')

'''
Output data options
'''
parser.add_argument('-o', '--output_directory',
                    default = '.',
                    type = str,
                    help = 'Path to the output directory (default = current directory).')

#Parse arguments to a dictionary.
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================
def print_date ():
    '''
    Print the current date and time to stderr.
    '''
    sys.stderr.write('-------------------\n')
    sys.stderr.write('{}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    sys.stderr.write('-------------------\n\n')
    return


def count(g = args['genotypes'], l = args['locdata'], n = args['names'], c  = args['completeness']):
    '''
    Count the number of loci, nucleotide sites, and SNPs.
    '''
    #Open genotypes and locdata file.
    gts = open(g)
    l = open(l)
    
    #Skip header of locdata file.
    l.readline()
    
    #Read header of genotypes file.
    header = gts.readline().rstrip().split('\t')[1:]
    
    #Define a dictionary with the locus IDs as keys and the locus lengths as values.
    locdata_dict = dict()
    for line in l:
        line = line.rstrip().split('\t')
        locdata_dict[line[0]] = int(line[1])
    
    #Open sample names list and convert into list.
    if n:
        names = list()
        n = open(n)
        for line in n:
            line = line.rstrip().split('\t')[0]
            names.append(line)
    
    #Create a list with the header indexes from the sample names in names list.
        indexes = list()
        for name in names:
            if name in header:
                indexes.append(header.index(name))
            else:
                assert name in header, '{} is not present in the genotypes file. Please check if the list with sample names contains typos or other errors.'
    
    #Count number of SNPs, loci, and nucleotides.
    nloci, nnucl, nSNP = 0, 0, 0
    for line in gts:
        line = line.rstrip().split('\t')
        if n:
            gts_line = [x for i, x in enumerate(line[1:]) if i in indexes and x != '-999']
        else:
            gts_line = line[1:]
        if len(gts_line) > 0:
            if (not n and len(gts_line) / len(line) >= c) or (n and len(gts_line) / len(names) >= c):
                nloci += 1
                nnucl += locdata_dict[line[0]]
            
            if 'consensus' not in gts_line:
                gts_line = [x.split('/') for x in gts_line]
                gts_line = [item for x in gts_line for item in x]
                if len(gts_line) > 0:
                    for i in range(len(gts_line[0])):
                        if not all(x[i] == gts_line[0][i] for x in gts_line):
                            nSNP += 1
    return nSNP, nloci, nnucl


#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':

    print_date()
    sys.stderr.write("* Counting the number of SNPs, loci, and nucleotides in {} ...\n".format(args['genotypes']))
    nSNP, nloci, nnucl = count()
    
    sys.stderr.write("\t{} SNPs were found over {} nucleotides in {} loci.\n".format(nSNP, nnucl, nloci))
    out_file = open('{}/Counts.txt'.format(args['output_directory']), 'w+')
    
    print('Genotypes file\tLocdata file\tnLoci\tnNucleotides\tnSNPs', file = out_file)
    print('{}\t{}\t{}\t{}\t{}'.format(args['genotypes'].split('/')[-1], args['locdata'].split('/')[-1], nloci, nnucl, nSNP), file = out_file)
    
    sys.stderr.write("* Finished! \n\n")
    print_date()
