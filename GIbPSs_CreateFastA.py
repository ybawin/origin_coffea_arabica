#!/usr/bin/python3

#===============================================================================
# GIbPSs_CreateFastA
#===============================================================================

# Yves Bawin, February 2019

#===============================================================================
# Import modules
#===============================================================================

import sys, argparse
from datetime import datetime

#===============================================================================
# Parse arguments
#===============================================================================

#Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Create a FastA file including all alleles delineated in the GIbPSs output files.')

#Add mandatory arguments
'''
Argument defining the name of the input file.
'''
parser.add_argument('--popall',
                    type = str,
                    help = 'File with all alleles delineated by GIbPSs (usually called "popall.txt").')

#Add optional arguments
'''
Input data options
'''
parser.add_argument('-i', '--input_directory',
                    default = '.',
                    type = str,
                    help = 'Input directory (default = current directory).')
parser.add_argument('--names',
                    type = str,
                    default = None,
                    help = 'A table with new sample names if you want to rename your samples: first column: old names, second column: new names (default = no list included).')

'''
Output data options
'''
parser.add_argument('-o', '--output_directory',
                    default = '.',
                    type = str,
                    help = 'Output directory (default = current directory).')

# Parse arguments to a dictionary
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================

def print_date ():
    '''
    Print the current date and time to stderr.
    '''
    sys.stderr.write('----------------\n')
    sys.stderr.write('{}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M')))
    sys.stderr.write('----------------\n\n')
    return

def CreateFastA(popall = args['popall'], names = args['names'], in_dir = args['input_directory'], out_dir = args['output_directory']):
    '''
    Convert the list of alleles delineated by GIbPSs into a FastA file.
    '''
    #Open the popall file.
    popall = open('{}/{}'.format(in_dir, popall))
    
    #Skip the first line (header).
    popall.readline()
    
    #Create an output file.
    out_file = open('{}/InputBLAST.fasta'.format(out_dir), 'w+')
    
    #Iterate over all remaining lines in the popall file and extract the poplocID and the allele sequence.
    for line in popall:
        line = line.rstrip().split('\t')
        
        #Print the allele to the output file in FastA format.
        print('>{}_{}\n{}'.format(line[0], line[1], line[2]), file = out_file)
    return


#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':
    
    print_date()
    
    sys.stderr.write(' * Converting the popall file into a FastA file ...\n')
    CreateFastA()
    sys.stderr.write(' * Finished!\n\n')
    
    print_date()