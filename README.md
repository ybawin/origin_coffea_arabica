# Scripts to manipulate the GIbPSs output files from "Phylogenomic analysis clarifies the evolutionary origin of *Coffea arabica*"

## Description

The Python scripts in this GitLab project were used to manipulate the GIbPSs output data to study the evolutionary origin of the allotetraploid species *Coffea arabica*. Five scripts are included:

1. **GIbPSs_CreateFastA.py** creates a FastA file including all alleles delineated in the GIbPSs output files.
2. **GIbPSs_FilterBLAST.py** removes loci from the BLAST output file with alleles that were identified as contaminants.
3. **GIbPSs_Counts.py** counts the number of loci, nucleotide sites, and SNPs in (a selection of samples) in the GIbPSs output files.
4. **GIbPSs_CalculateGeneticRelatedness.py** calculates pairwise genetic similarity or genetic distance values between all specified samples in the GIbPSs output files.
5. **GIbPSs_SplitHybridLoci.py** splits the genome of hybrid samples in the GIbPSs output files based on genetic similarity with groups of putative progenitor samples and creates a (set of) nucleotide alignment(s).

## Citation

Bawin *et al*. (under review). Phylogenomic analysis clarifies the evolutionary origin of *Coffea arabica*.

## Requirements

All python scripts were developed in python3.6.  
Further requirements are listed, tagged per component: <sup>1</sup> GIbPSs_CreateFastA.py, <sup>2</sup>GIbPSs_FilterBLAST.py, <sup>3</sup> GIbPSs_Counts.py, <sup>4</sup> GIbPSs_CalculateGeneticRelatedness.py, <sup>5</sup>GIbPSs_SplitHybridLoci.py.

sys<sup>1,2,3,4,5</sup>, argparse<sup>1,2,3,4,5</sup>, datetime<sup>1,2,3,4,5</sup>, os<sup>2,4,5</sup>, math<sup>4</sup>, random<sup>4</sup>, numpy<sup>4</sup>, matplotlib<sup>4</sup>, shelve<sup>4</sup>, seaborn<sup>4</sup>.


## Building and installing

### 1. GIbPSs_CreateFastA.py

The script GIbPSs_CreateFastA.py counts the number of loci, nucleotide sites, and SNPs in (a selection of samples) in the GIbPSs output files. 

#### Usage

`python3 GIbPSs_CreateFastA.py --popall POPALL [-i INPUT_DIRECTORY] [--names NAMES] [-o OUTPUT_DIRECTORY]`

**Mandatory argument**

    --popall  STR  File with all alleles delineated by GIbPSs (usually called "popall.txt").

**Input data options**

	-i, --input_directory   STR	  Input directory [current directory].
	--names                 STR	  A table with new sample names if you want to rename your samples: first column: old names, second column: new names [no list included].

**Output data options**

    -o, --output_directory  STR	  Output directory [current directory].

### 2. GIbPSs_FilterBLAST.py

The script GIbPSs_FilterBLAST.py removes loci from the BLAST output file with alleles that were identified as contaminants.

#### Usage

`python3 GIbPSs_FilterBLAST.py -f FILE -l LOCDATA [-o OUTPUT_DIRECTORY]`

**Mandatory argument**

    -f, --file        STR  Path to the input file containing all alleles that were not identified as contaminants after BLAST (e.g. OutputBLAST_filtered.txt).
    -l, --locdata     STR  Path to the locdata file (usually called locdata.txt).

**Output data options**

    -o, --output_directory  STR	  Output directory [current directory].

### 3. GIbPSs_Counts.py

The script GIbPSs_Counts.py creates a FastA file including all alleles delineated in the GIbPSs output files.

#### Usage

`python3 GIbPSs_Counts.py -gt GENOTYPES -l LOCDATA [--names NAMES] [-c COMPLETENESS] [-o OUTPUT_DIRECTORY]`

**Mandatory argument**

    -gt, --genotypes  STR  Path to the genotypes file (usually called genotypes.txt).
    -l, --locdata     STR  Path to the locdata file (usually called locdata.txt).

**Input data options**

	--names           STR  List of all sample names (tab delimited text file) that must be taken into account for locus and SNP counts [no list included].
	
**Analysis options**

	-c, --completeness FLOAT  Filter GIbPSs output files for locus completeness: loci with data in less samples than the predefined proportion are removed [no filtering].

**Output data options**

    -o, --output_directory  STR	  Output directory [current directory].

### 4. GIbPSs_CalculateGeneticRelatedness.py

The script GIbPSs_CalculateGeneticRelatedness.py estimates the pairwise genetic similarity between accessions in the genotypes output file of GIbPSs. Genetic similarity is expressed as the following coefficients:

Jaccard = a / (a + b + c) (Jaccard, 1912)

Sorensen-Dice = (2 * a) / (2 * a + b + c) (Dice, 1945; Sørensen, 1948)

Ochiai = a /sqrt((a + b) * (a + c)) (Ochiai, 1957)

Common_allele = d / e

With *a* the number of shared alleles between accession 1 and 2, *b* the number of unique alleles in accession 1, *c* the number of unique alleles in accession 2, *d* the number of loci with at least one shared allele, and *e* the number of loci. The number of shared/unique alleles is expressed as the incidence (*i.e.* presence/absence) of alleles in one or both samples. Pairwise similarity estimates can be converted into genetic distances using the inversed or the Euclidean transformation method:

Inversed = 1 - similarity

Euclidean = sqrt((1 - similarity)^2)

By default, alleles in loci with data in only one out of two samples in a sample pair are not taken into account in genetic similarity/distance calculations.
The option (--include_unique_loci) overrules this default behaviour, allowing to use alleles in non-shared loci next to the alleles in shared loci for genetic similarity/distance estimates. However, the use of data from non-shared loci can only be considered if the amount of exogenous contamination and the amount of missing data due to technical causes (*e.g.* low read depth) is very low. In this case, the absence of data in a region might have a biological explanation (*e.g.* high sequence divergence compared to the reference genome sequence). Including this information may reveal additional patterns of genetic clustering if the resolution in the sequence data on shared loci is not high enough.

If the genotypes table has not been used as input for the construction of a genetic similarity/distance matrix before, it is recommended to first plot curves that show genetic similarity/distance estimates in function of the number of loci for all relevant sample pairs by providing a list with the relevant sample pairs that must be included in the curves via the option --plot-list. These plots show the minimum required number of shared loci between two samples to obtain stable genetic similarity/distance estimates. This information can be used to identify samples with not enough data, which can be excluded from the analysis afterwards.

The resulting genetic similarity/distance matrix can be exported in phylip or Nexus format for downstream analyses, or directly as a PNG, PDF, or SVG graph for publication purposes. Genetic distance matrices in Phylip format with data on both sides of the diagonal (mask_upper = False) can directly be imported in R (R core team, 2020) to reconstruct PCoA plots with the *cmdscale* function or a distance-based phylogenetic tree (*e.g.* with the *nj* function in the R package ape (Paradis & Schliep, 2018)). Genetic distance matrices in Nexus format with data on both sides of the diagonal can be used to reconstruct distance-based phylogenetic networks with the program SplitsTree (Huson & Bryant, 2006).

Users can adjust the default thresholds of filters throughout the procedure. See detailed description of the workflow for explanation of the filter parameters.

#### Usage

`python3 GIbPSs_CalculateGeneticRelatedness.py -gt GENOTYPES [-i INPUT_DIRECTORY] [--names NAMES] [--similarity_coefficient {'Jaccard', 'Sorensen-Dice', 'Ochiai', 'Common_allele'}] [--distance] [--distance_method {Standard, Euclidean}] [-o OUTPUT_DIRECTORY] [--format {Phylip, Nexus}] [--plot] [--plot_interval PLOT_INTERVAL] [--plot_format {pdf, png, svg}] [--mask_upper] [--annotate_heat_map] [step specific options for filtering]`

**Mandatory argument**

    -gt, --genotypes  STR  Path to the genotypes file (usually called genotypes.txt).

**Input data options**


	-i, --input_directory   STR	  Input directory [current directory].
	--names                 STR	  List (tab delimited text file) with (a subset of) the sample names in the first column and the new sample names in the second column [no list provided].

**Analysis options**

	--include_unique_loci        Include loci in the genetic similarity/distance calculations that are unique for one of the samples in a sample pair [only common loci are used for similarity/distance calculation].
	-s, --similarity_coefficient {Jaccard, Sorensen-Dice, Ochiai, Common_allele}  STR  Coefficient used to express pairwise genetic similarity between samples [Jaccard].
	--distance                   Convert genetic similarity estimates into genetic distances [no conversion to distances].
	--distance_method            {Inversed, Euclidean}                            STR  Method used for conversion of similarity estimates to genetic distances [Inversed].

**Output data options**

	-o, --output_directory        STR  Output directory [current directory].
	--format {Phylip, Nexus}      STR  The format of the genetic similarity or distance matrix [Phylip].
	--plot                             Create plots of the similarity/distance matrix [no plots].
	--plot_list                   STR  Use this option if you want to plot the genetic relatedness of a sample to all other samples in function of the number of loci. This option needs a list (tab-delimited text file) with the sample names for which plots must be created [no list included].
	--plot_interval               INT  Interval between two consecutive points in the plot [10].
	--plot_format {pdf, png, svg} STR  File format of the similarity/distance matrix and/or plots [pdf].
	--mask_upper          	           Mask values on and above the main diagonal of the similarity/distance matrix [no masking].
	--annotate_heat_map                Annotate the heat map with genetic similarity/distance values [heatmap not annotated].

### 5. GIbPSs_SplitHybridLoci.py

The script GIbPSs_SplitHybridLoci.py splits the genome of hybrid samples in the GIbPSs output files based on genetic similarity with groups of putative progenitor samples. These groups must be delineated in a tab delimited text file with each column representing one putative progenitor lineage. The output is a set of locus alignments or one concatenated alignment which can be used as input for phylogenetic analyses, for instance with RAxML v8 (Stamatakis, 2014) or BEAST v1.10 (Suchard *et al*., 2018).

#### Usage

`python3 GIbPSs_Counts.py -gt GENOTYPES -l LOCDATA --progenitors PROGENITORS --hybrid HYBRID [--names NAMES] [--ploidy] [-c COMPLETENESS] [--polymorphic] [--all_subgenomes] [--remove] [--taxa] [--rename] [-o OUTPUT_DIRECTORY] [--alignment_format {FastA, Phylip, Nexus}] [--separate_alignments] [--suffix SUFFIX] [--delete_intermediate]`

**Mandatory argument**

    -gt, --genotypes  STR  Path to the genotypes file (usually called genotypes.txt).
    -l, --locdata     STR  Path to the locdata file (usually called locdata.txt).
    --progenitors     STR  Path to a table (tab-delimited text file) with each column containing the sample names that belong to the same progenitor group (no header included).
    --hybrid          STR  Path to a list (tab-delimited text file) with the sample names of a hybrid species (no header included).

**Input data options**

	--names            STR	  List with the new sample names if the rename option is used: first column = old names, second column = new names [no list included].
	--ploidy           INT	  Ploidy level of the progenitor species [2 (diploid)].
	
**Analysis options**

	-c, --completeness FLOAT  Filter GIbPSs output files for locus completeness: loci with data in less samples than the predefined proportion are removed [no filtering].
	--polymorphic             Ignore non-polymorphic loci [non-polymorphic loci are not ignored].
	--all_subgenomes          Only retain loci of hybrid samples if data are available for each subgenome [False].
	--remove                  Remove taxa from the genotypes file. Only taxa that are listed in the file that is specified in the option "taxa", are kept [no taxa removed].
	--taxa                    List (tab-delimited text file) with sample names that must be retained [no list provided].
	--rename                  Rename samples as specified in the --taxa file: first column = old name, second column = new name [samples are not renamed].
	
**Output data options**

    -o, --output_directory                        STR   Output directory [current directory].
    --alignment_format {FastA, Phylip, Nexus}     STR   The format of the output alignment [FastA].
    --separate_alignments                               Create separate locus alignments [concatenated alignment].
    --suffix                                      STR   Suffix for genotypes and locdata file after the split of hybrid genotypes [No suffix used].
    --delete_intermediate                               Delete the filtered genotypes and locdata file [intermediate files are not deleted].

## References
Dice, L. R. (1945). Measures of the Amount of Ecological Association between Species. *Ecology, 26*(3), 297-302. https://doi.org/10.2307/1932409

Huson, D. H. & Bryant, D. (2006). Application of Phylogenetic Networks in Evolutionary Studies. *Molecular Biology and Evolution, 23*(2), 254-267. https://doi.org/10.1093/molbev/msj030

Jaccard, P. (1912). The distribution of the flora in the Alpine zone. *New Phytologist, 11*(2), 37-50. https://doi.org/10.1111/j.1469-8137.1912.tb05611.x

Ochiai, A. (1980). Zoogeographical Studies on the Soleoid Fishes Found in Japan and Its Neighbouring Regions - II. *Bulletin of the Japanese Society of Scientific Fisheries, 22*(9), 526–530. https://doi.org/10.15080/agcjchikyukagaku.58.4_256

Paradis, E. & Schliep, K. 2018. ape 5.0: an environment for modern phylogenetics and evolutionary analyses in R. *Bioinformatics 35*(3), 526-528. https://doi.org/10.1093/bioinformatics/bty633


R Core Team (2020). R: A language and environment for statistical computing. R Foundation for Statistical Computing, Vienna, Austria. URL https://www.R-project.org/

Sørensen, T. (1948). A Method of Establishing Groups of Equal Amplitude in Plant Sociology based on Similarity of Species Content and its Application to Analyses of the Vegetation on Danish Commons. *Kongelige Danske Videnskabernes Selskab, 5*(4), 1–34.

Stamatakis, A. (2014). RAxML Version 8: A tool for Phylogenetic Analysis and Post-Analysis of Large Phylogenies. *Bioinformatics, 30*(9), 1312–1313. https://doi.org/10.1093/bioinformatics/btu033

Suchard M. A., Lemey P., Baele G., Ayres D. L., Drummond A. J., Rambaut A. (2018). Bayesian phylogenetic and phylodynamic data integration using BEAST 1.10. *Virus Evolution 4*(1), 1–5. https://doi.org/10.1093/ve/vey016