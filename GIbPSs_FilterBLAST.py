#!usr/bin/python3

#===============================================================================
# GIbPSs_FilterBLAST
#===============================================================================

# Yves BAWIN November 2018

#===============================================================================
# Import modules
#===============================================================================

import os, sys, argparse
from datetime import datetime

#===============================================================================
# Parse arguments
#===============================================================================

# Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Remove loci from the BLAST output file with alleles that were identified as contaminants.')

#Mandatory arguments

'''
Arguments defining the file paths leading to the filtered BLAST output file and the locdata file.
'''
parser.add_argument('-f', '--file',
                    type = str,
                    help = 'Path to the input file containing all alleles that were not identified as contaminants after BLAST (e.g. OutputBLAST_filtered.txt).')

parser.add_argument('-l', '--locdata',
                    type = str,
                    help = 'Path to the locdata file (usually called locdata.txt)')

# Optional arguments

'''
Output data options
'''

parser.add_argument('-o', '--out_dir',
                    default = '.',
                    type = str,
                    help = 'Output directory (default = current directory).')

# Parse arguments to a dictionary
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================

def print_date ():
    """
    Print the current date and time to stderr.
    """
    sys.stderr.write('-------------------\n')
    sys.stderr.write('{}\n'.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
    sys.stderr.write('-------------------\n\n')
    return


def Filter(file = args['file'], locdata = args['locdata'], out_dir = args['out_dir']):
    '''
    Remove loci from the BLAST output file with alleles from contaminants.
    '''
    #Open the input files.
    file = open(file)
    locdata = open(locdata)
    
    #Create an output file.
    BLAST_filtered = open('{}/Coffea_alleles.txt'.format(out_dir), 'w+')
    
    #Create a dictionary containing all alleles in the input file.
    alleles = dict()
    for line in file:
        line = line.rstrip().split('_')
        if line[0] not in alleles:
            alleles[line[0]] = 1
        else:
            alleles[line[0]] += 1
    
    #Check if all alleles of a locus are identified.
    for line in locdata:
        line = line.rstrip().split('\t')
        if line[0] in alleles and int(line[5]) == int(alleles[line[0]]):
            print(line[0], file = BLAST_filtered)
    return


#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':

    print_date()
    sys.stderr.write("* Filtering the BLAST output file ...\n")
    Filter()
    sys.stderr.write("* Finished! \n\n")
    print_date()
                        